import { reduxType } from "../../Constants/Constants"


const setPostList = (postList) => {
    return {
        type: reduxType.SET,
        payload: { postList }
    }
}

const addPost = (post) => {
    return {
        type: reduxType.ADD,
        payload: { post }
    }
}

const updatePost = (post) => {
    return {
        type: reduxType.UPDATE,
        payload: { post }
    }
}

const deletePost = (postID) => {
    return {
        type: reduxType.DELETE,
        payload: { postID }
    }
}

const postActions = {
    setPostList,
    addPost,
    updatePost,
    deletePost
}

export default postActions;