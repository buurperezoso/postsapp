import { reduxType } from "../../Constants/Constants";

const initialState = {
    postList: ''
}

const postListReducer = (state = initialState, action) => {
    const postList = state.postList;
    switch (action.type) {
        case reduxType.SET:
            return {
                ...state,
                postList: action.payload.postList
            };

        case reduxType.ADD:
            return {
                ...state,
                postList: [action.payload.post, ...postList]
            };

        case reduxType.UPDATE:
            return {
                ...state,
                postList: postList.map((post) => {
                    if (post.id === action.payload.post.id) {
                        post = action.payload.post;
                    }

                    return post;
                })
            };

        case reduxType.DELETE:
            return {
                ...state,
                postList: postList.filter((post) => post.id !== action.payload.postID)
            };

        default:
            return state
    }
};

export default postListReducer;