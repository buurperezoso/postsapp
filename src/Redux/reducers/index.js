import { combineReducers } from 'redux';
import postListReducer from './PostListReducer';

const rootReducer = combineReducers({
    postListReducer
});

export default rootReducer;