import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { LoginButton, NewPostButton } from './styles/GlobalComponentStyles';

import { Col, Container, Modal, Row, Image } from 'react-bootstrap';
import { TextField } from '@material-ui/core';

import { DeletePostButton } from '../components/styles/GlobalComponentStyles';

import imageSad from '../assets/img/sad.gif';

const Login = () => {

    const userName = localStorage.getItem('user');

    const [showModalLogin, setShowModalLogin] = useState(false);
    const [showModalLogout, setShowModalLogout] = useState(false);

    const [user, setUser] = useState('');
    const [userError, setUserError] = useState(false);

    useEffect(() => {

        if (localStorage.getItem('user')) {
            setShowModalLogin(false);
        } else {
            setShowModalLogin(true);
        }

    }, []);

    const saveUser = useCallback((event) => {
        event.preventDefault();

        let error = false;

        if (!user) {
            setUserError(true);
            error = true;
        } else {
            setUserError(false);
        }

        if (!error) {
            localStorage.setItem('user', user);
            setShowModalLogin(false);
        }
    }, [user]);

    const handleCloseLogin = useCallback(() => {
        setShowModalLogin(false);
    }, []);

    const handleCloseLogout = useCallback(() => {
        setShowModalLogout(false);
    }, []);

    const styleContainer = {
        paddingTop: '2rem',
        paddingBottom: '2rem'
    }

    return (
        <Fragment>
            <LoginButton type='button' onClick={() => setShowModalLogout(true)}>
                <FontAwesomeIcon icon="user" />
            </LoginButton>

            <Modal
                show={showModalLogin}
                onHide={handleCloseLogin}
                centered
                backdrop="static">
                <Container className='pl-sm-5 pr-sm-5' style={styleContainer}>

                    <Row>
                        <Col className='d-flex justify-content-center'>
                            <div style={{ width: '10rem', height: 'auto' }}>
                                <Image src='https://i.pinimg.com/originals/a6/58/32/a65832155622ac173337874f02b218fb.png' fluid />
                            </div>
                        </Col>
                    </Row>

                    <form autoComplete='off' onSubmit={saveUser} className='ml-auto mr-auto' style={{ maxWidth: '15rem' }}>

                        <Row>
                            <Col>
                                <TextField
                                    error={userError}
                                    fullWidth
                                    label="User"
                                    value={user}
                                    onChange={e => {
                                        setUser(e.target.value);
                                        if (e.target.value) {
                                            setUserError(false);
                                        } else {
                                            setUserError(true);
                                        }
                                    }}
                                    helperText={userError && 'This field is requiered.'}
                                />
                            </Col>
                        </Row>

                        <Row>
                            <Col className='d-flex justify-content-center mt-4'>
                                <NewPostButton type='submit'>
                                    Login
                                </NewPostButton>
                            </Col>
                        </Row>

                    </form>

                </Container>
            </Modal>

            <Modal
                show={showModalLogout}
                onHide={handleCloseLogout}
                centered
                backdrop="static">
                <Container className='pl-sm-5 pr-sm-5' style={styleContainer}>

                    <Row>
                        <Col xs={12} className='text-left'>
                            <span className='h3'><span style={{ color: 'coral' }}>{userName}</span>, are you sure you want to logout?</span>
                        </Col>
                    </Row>

                    <Row>
                        <Col>
                            <div className='ml-auto mr-auto mt-3' style={{ maxWidth: '17rem' }}>
                                <Image style={{ borderRadius: '1rem' }} src={imageSad} fluid />
                            </div>
                        </Col>
                    </Row>

                    <Row className='mt-3'>

                        <Col className='d-none d-sm-flex justify-content-end'>
                            <div>

                                <button
                                    type='button'
                                    className='btn mr-4'
                                    style={{ width: '5.25rem', marginRight: '1.5rem' }}
                                    onClick={handleCloseLogout}
                                >
                                    Cancel
                                </button>

                                <DeletePostButton
                                    type='button'
                                    style={{ width: '5.25rem' }}
                                    onClick={() => {
                                        localStorage.removeItem('user');
                                        handleCloseLogout();
                                        setTimeout(() => {
                                            setUser('');
                                            setUserError(false);
                                            setShowModalLogin(true);
                                        }, 500)
                                    }}
                                >
                                    Logout
                                </DeletePostButton>

                            </div>
                        </Col>

                        <Col className='d-block d-sm-none'>
                            <Row>
                                <Col xs={6} className='d-flex justify-content-end'>
                                    <button
                                        type='button'
                                        className='btn mr-4'
                                        style={{ width: '5.25rem', marginRight: '1.5rem' }}
                                        onClick={handleCloseLogout}
                                    >
                                        Cancel
                                </button>
                                </Col>
                                <Col xs={6}>
                                    <DeletePostButton
                                        type='button'
                                        style={{ width: '5.25rem' }}
                                        onClick={() => {
                                            localStorage.removeItem('user');
                                            handleCloseLogout();
                                            setTimeout(() => {
                                                setUser('');
                                                setUserError(false);
                                                setShowModalLogin(true);
                                            }, 500)
                                        }}
                                    >
                                        Logout
                                </DeletePostButton>
                                </Col>
                            </Row>
                        </Col>

                    </Row>

                </Container>
            </Modal>
        </Fragment>
    );
}

export default Login;