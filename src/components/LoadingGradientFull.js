import React from 'react';

import styles from './styles/LoadingGradient.module.css';

const LoadingGradientFull = () => {

    return (
        <div className='mt-5'>
            <div className={styles.gradientDiv}></div>
        </div>
    );
}

export default LoadingGradientFull;