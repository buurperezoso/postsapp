import React, { useState, useCallback } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import CircularProgress from '@material-ui/core/CircularProgress';

import { DeletePostButton } from './styles/GlobalComponentStyles';

import { devAPIurl, httpStatus, reduxType } from '../Constants/Constants';
import { connect, useDispatch } from 'react-redux';
import allActions from '../Redux/actions';

const ModalDeletePost = ({ clickOnCancel, postID }) => {

    // Http request state (controls button delete disabled and label)
    const [httpReqState, setHttpReqState] = useState(httpStatus.NONE);

    const dispatch = useDispatch();

    const deletePost = useCallback((postID) => {

        const url = `${devAPIurl}/post/${postID}/delete`;
        const options = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8'
            },
            crossDomain: true,
            body: JSON.stringify({ id: postID }),
        };

        fetch(url, options)
            .then(response => {
                dispatch(allActions.postActions.deletePost(postID));
                setHttpReqState(httpStatus.RESPONSE);
                clickOnCancel();

            }).catch((error) => {
                console.error(error)
                setHttpReqState(httpStatus.RESPONSE);
            });

    }, [dispatch, clickOnCancel]);

    const styleContainer = {
        paddingLeft: '4rem',
        paddingRight: '4rem',
        paddingTop: '2rem',
        paddingBottom: '2rem'
    }

    return (
        <Container style={styleContainer}>
            <Row>
                <Col xs={12} className='text-left'>
                    <span className='h3'>Are you sure you want to delete this post?</span>
                </Col>
            </Row>

            <Row className='mt-5'>

                <Col className='justify-content-end d-sm-flex d-none'>
                    <div>

                        <button
                            type='button'
                            className='btn mr-4'
                            style={{ width: '5.25rem', marginRight: '1.5rem' }}
                            onClick={clickOnCancel}
                            disabled={httpReqState === 'SENT'}
                        >
                            Cancel
                        </button>

                        <DeletePostButton
                            type='button'
                            style={{ width: '5.25rem' }}
                            onClick={() => {
                                setHttpReqState(httpStatus.SENT);
                                deletePost(postID);
                            }}
                            disabled={httpReqState === httpStatus.SENT}
                        >
                            {
                                httpReqState === httpStatus.SENT ? (
                                    <CircularProgress
                                        size={'1.1rem'}
                                        style={{ color: 'white' }}
                                    />
                                ) : 'Delete'
                            }
                        </DeletePostButton>

                    </div>
                </Col>

                <Col className='d-block d-sm-none'>
                    <Row>
                        <Col xs={6} className='d-flex justify-content-end'>
                            <button
                                type='button'
                                className='btn mr-4'
                                style={{ width: '5.25rem', marginRight: '1.5rem' }}
                                onClick={clickOnCancel}
                                disabled={httpReqState === 'SENT'}
                            >
                                Cancel
                        </button>
                        </Col>
                        <Col xs={6}>
                            <DeletePostButton
                                type='button'
                                style={{ width: '5.25rem' }}
                                onClick={() => {
                                    setHttpReqState(httpStatus.SENT);
                                    deletePost(postID);
                                }}
                                disabled={httpReqState === httpStatus.SENT}
                            >
                                {
                                    httpReqState === httpStatus.SENT ? (
                                        <CircularProgress
                                            size={'1.1rem'}
                                            style={{ color: 'white' }}
                                        />
                                    ) : 'Delete'
                                }
                            </DeletePostButton>
                        </Col>
                    </Row>
                </Col>

            </Row>

        </Container>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        onDeletePost: (postID) => dispatch({ type: reduxType.DELETE, payload: { postID } })
    };
};

export default connect(null, mapDispatchToProps)(ModalDeletePost);