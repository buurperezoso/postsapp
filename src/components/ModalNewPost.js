import React, { useCallback, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
    MenuItem,
    TextField,
    FormControl,
    InputLabel,
    Select,
    Input,
    InputAdornment,
    IconButton,
    FormHelperText,
} from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { NewPostButton } from './styles/GlobalComponentStyles';

import {
    categoryButtons,
    devAPIurl,
    httpStatus,
    urlRegex
} from '../Constants/Constants';
import { useDispatch } from 'react-redux';
import allActions from '../Redux/actions';

const ModalNewPost = ({ clickOnCancel }) => {

    // Form states
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState('');
    const [imgRoute, setImgRoute] = useState('');

    //Form errors
    const [titleError, setTitleError] = useState(false);
    const [descriptionError, setDescriptionError] = useState(false);
    const [categoryError, setCategoryError] = useState(false);
    const [imgRouteError, setImgRouteError] = useState('VALID');

    // Http request state (controls button save disabled and label)
    const [httpReqState, setHttpReqState] = useState(httpStatus.NONE);

    const dispatch = useDispatch();

    // Http request handler
    const savePost = useCallback((formData) => {

        const url = `${devAPIurl}/create-post`;
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8'
            },
            crossDomain: true,
            body: JSON.stringify(formData),
        };

        fetch(url, options)
            .then(response => {
                return response.json();
            }).then(
                (responseData) => {
                    dispatch(allActions.postActions.addPost(responseData));
                    setHttpReqState(httpStatus.RESPONSE);
                    clickOnCancel(); // Trigger the function to close modal after done
                }
            ).catch((error) => {
                setHttpReqState(httpStatus.RESPONSE);
                console.error(error);
            });
    }, [dispatch, clickOnCancel]);

    // Form handler
    const handleSubmit = useCallback((event) => {
        event.preventDefault();

        let error = false;

        if (!title) {
            setTitleError(true);
            error = true;
        } else {
            setTitleError(false);
        }

        if (!description) {
            setDescriptionError(true);
            error = true;
        } else {
            setDescriptionError(false);
        }

        if (!category) {
            setCategoryError(true);
            error = true;
        } else {
            setCategoryError(false);
        }

        if (!imgRoute) {
            setImgRouteError('INVALID');
            error = true;
        } else if (!urlRegex.test(imgRoute)) {
            setImgRouteError('URL_INVALID');
            error = true;
        } else {
            setImgRouteError('VALID');
        }

        if (!error) {
            const formData = {
                title: title,
                description: description,
                category: category,
                imgRoute: imgRoute,
                user: localStorage.getItem('user')
            }

            setHttpReqState(httpStatus.SENT);
            savePost(formData);
        }

    }, [title, description, category, imgRoute, savePost]);

    return (
        <Container
            className='pl-sm-5 pr-sm-5'
            style={{
                paddingTop: '2rem',
                paddingBottom: '2rem'
            }}>
            <Row>
                <Col xs={12} className='text-center'>
                    <span className='h3'>Create Post</span>
                </Col>
            </Row>

            <form autoComplete='off' onSubmit={handleSubmit} className='ml-auto mr-auto' style={{ maxWidth: '20rem' }}>
                <Row className='mt-4'>
                    <Col xs={12} className='form-group '>
                        <TextField
                            error={titleError}
                            fullWidth
                            label="Title"
                            value={title}
                            onChange={e => {
                                setTitle(e.target.value);
                                if (e.target.value) {
                                    setTitleError(false);
                                } else {
                                    setTitleError(true);
                                }
                            }}
                            helperText={titleError && 'This field is requiered.'}
                        />
                    </Col>
                    <Col xs={12} className='form-group'>
                        <TextField
                            error={descriptionError}
                            fullWidth
                            label="Description"
                            multiline
                            rows={2}
                            value={description}
                            onChange={e => {
                                setDescription(e.target.value);
                                if (e.target.value) {
                                    setDescriptionError(false);
                                } else {
                                    setDescriptionError(true);
                                }
                            }}
                            helperText={descriptionError && 'This field is requiered.'}
                        />
                    </Col>
                    <Col xs={12} className='form-group'>
                        <FormControl fullWidth>
                            <InputLabel id="category" error={categoryError}>Category</InputLabel>
                            <Select
                                error={categoryError}
                                labelId="category"
                                value={category}
                                onChange={e => {
                                    setCategory(e.target.value);
                                    if (e.target.value) {
                                        setCategoryError(false);
                                    } else {
                                        setCategoryError(true);
                                    }
                                }}
                            >
                                {
                                    categoryButtons.map((category, index) => {
                                        return (
                                            <MenuItem key={index} value={category.toUpperCase()}>{category}</MenuItem>
                                        )
                                    })
                                }

                            </Select>
                            {   // Error message
                                categoryError && <FormHelperText style={{ color: 'red' }}>This field is requiered.</FormHelperText>
                            }
                        </FormControl>
                    </Col>
                    <Col xs={12} className='form-group'>

                        <FormControl fullWidth>
                            <InputLabel htmlFor="imgRoute" error={imgRouteError !== 'VALID'}>URL of the image</InputLabel>
                            <Input
                                error={imgRouteError !== 'VALID'}
                                id="imgRoute"
                                type='text'
                                value={imgRoute}
                                onChange={e => {
                                    setImgRoute(e.target.value);
                                    if (e.target.value) {
                                        setImgRouteError('VALID');
                                    } else {
                                        setImgRouteError('INVALID');
                                    }
                                }}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton style={{ outline: 'none' }}>
                                            <FontAwesomeIcon icon="link" />
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                            {   // Error message
                                (() => {
                                    switch (imgRouteError) {
                                        case 'INVALID':
                                            return <FormHelperText style={{ color: 'red' }}>This field is requiered.</FormHelperText>;
                                        case 'VALID':
                                            return '';
                                        case 'URL_INVALID':
                                            return <FormHelperText style={{ color: 'red' }}>Invalid image url.</FormHelperText>;
                                        default:
                                            throw new Error('Invalid action');
                                    }
                                })()
                            }

                        </FormControl>

                    </Col>

                    <Col className='mt-5 justify-content-end d-none d-sm-flex'>
                        <div>

                            <button
                                type='button'
                                className='btn mr-4'
                                style={{ width: '5.25rem', marginRight: '1.5rem' }}
                                onClick={clickOnCancel}
                                disabled={httpReqState === httpStatus.SENT}
                            >
                                Cancel
                            </button>

                            <NewPostButton
                                disabled={httpReqState === httpStatus.SENT}
                                type='submit'
                                style={{ width: '5.25rem' }}
                            >
                                {
                                    httpReqState === httpStatus.SENT ? (
                                        <CircularProgress
                                            size={'1.1rem'}
                                            style={{ color: 'white' }}
                                        />
                                    ) : 'Save'
                                }
                            </NewPostButton>

                        </div>
                    </Col>

                    <Col className='mt-5 d-block d-sm-none'>
                        <Row>
                            <Col xs={6} className='d-flex justify-content-end'>
                                <button
                                    type='button'
                                    className='btn mr-4'
                                    style={{ width: '5.25rem', marginRight: '1.5rem' }}
                                    onClick={clickOnCancel}
                                    disabled={httpReqState === httpStatus.SENT}
                                >
                                    Cancel
                                </button>
                            </Col>
                            <Col xs={6}>
                                <NewPostButton
                                    disabled={httpReqState === httpStatus.SENT}
                                    type='submit'
                                    style={{ width: '5.25rem' }}
                                >
                                    {
                                        httpReqState === httpStatus.SENT ? (
                                            <CircularProgress
                                                size={'1.1rem'}
                                                style={{ color: 'white' }}
                                            />
                                        ) : 'Save'
                                    }
                                </NewPostButton>
                            </Col>
                        </Row>
                    </Col>

                </Row>
            </form>

        </Container>
    );
}

export default ModalNewPost;