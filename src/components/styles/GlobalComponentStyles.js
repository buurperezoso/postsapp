import styled from 'styled-components';

export const NewPostButton = styled.button`
        display: inline-block;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        background-color: #303F84;
        user-select: none;
        border: 1px solid transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        color: white;
        :hover {
            background-color: #242E5D;
            color: white;
        }

        :focus{
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
        }
    `;

export const DeletePostButton = styled(NewPostButton)`
        background-color: #c50000;
        :hover {
            background-color: #9a0000;
            color: white;
        }
    `;

export const LoginButton = styled.button`
        display: inline-block;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        -webkit-user-select: none;
        user-select: none;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: 50%;
        box-shadow: 0 0.4rem 0.3rem #9E9E9E!important;
        border: none;
        height: 3rem;
        width: 3rem;
        color: rgb(138, 138, 138);
        transition: .4s;
        :hover {
            background-color: #242E5D;
            color: white;
            transform: scale(1.10);
        }

        :focus{
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
        }
    `;