import React, { Fragment, useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { useParams } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

import PostComments from './components/PostComments';
import LoadingGradientFull from '../../components/LoadingGradientFull';
import { useSelector } from 'react-redux';

import { httpStatus } from '../../Constants/Constants';

const PostSelected = ({ history }) => {

    // Array obtained from redux
    const postList = useSelector(state => state.postListReducer.postList);

    // ID Post from url params
    const { postID } = useParams();

    // State variable that contains the post selected
    const [postObj, setPostObj] = useState('');

    // Http request state (controls loading animation)
    const [httpReqState, setHttpReqState] = useState(httpStatus.SENT);

    useEffect(() => {
        window.scrollTo(0, 0); // Starts at the top of the view when is firsts render
    }, []);

    useEffect(() => { // If view starts with post array value from redux
        if (Array.isArray(postList)) {
            const post = postList.find((post) => post.id === postID);
            if (post) {
                document.title = post.title;
                setPostObj(post);
                setHttpReqState(httpStatus.RESPONSE);
            } else {
                history.push('/');
            }
        }

    }, [postList, history, postID]);

    const stylesImagesContainer = {
        minHeight: '60vh',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        color: 'white',
        padding: '1rem',
        boxShadow: '0 0 0 1000px rgba(0, 0, 0, 0.4) inset'
    }

    const content = (
        <Container
            fluid
            className='mt-5 pb-3 fade-in'
            style={{ backgroundColor: '#efefef' }}
        >
            <Row style={{
                backgroundImage: `url(${postObj.imgRoute}`,
                ...stylesImagesContainer
            }}>
                <Link to="/" style={{ color: 'white' }}>
                    <FontAwesomeIcon
                        className='align-middle'
                        style={{ fontSize: '1.6rem' }}
                        icon="angle-left"
                    />
                    <span className='ml-2'>View Posts</span>
                </Link>
                <Col xs={12}>
                    <span style={{ fontSize: '3rem' }}>{postObj.title}</span>
                </Col>
            </Row>

            <Container>
                <Row>
                    <Col xs={12} className='mt-4 text-justify'>
                        <p style={{ fontSize: '1.5rem' }}>{postObj.description}</p>
                    </Col>
                </Row>

                <PostComments postID={postObj.id} comments={postObj.comments}></PostComments>

            </Container>
        </Container >
    );

    const loader = (
        <LoadingGradientFull />
    );

    return (
        <Fragment>
            {
                (() => {
                    switch (httpReqState) {
                        case httpStatus.SENT:
                            return loader;
                        default:
                            return content;
                    }
                })()
            }
        </Fragment>
    );
}

export default PostSelected;