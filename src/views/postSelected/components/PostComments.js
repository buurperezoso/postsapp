import React from 'react';
import { Container } from 'react-bootstrap';

import Comment from './Comment';
import AddComment from './AddComment';

const PostComments = (props) => {

    const { postID, comments } = props;

    return (
        <Container className='ml-auto mr-auto mt-3 text-left' style={{ maxWidth: '50rem' }}>
            <span style={{ fontWeight: 'bold', fontSize: '1.2rem' }}>Comments</span>

            {
                comments.map(
                    (comment) => {
                        return <Comment comment={comment} key={comment.id} author={comment.author}></Comment>
                    }
                )
            }

            <AddComment postID={postID}></AddComment>

        </Container>
    );
}

export default PostComments;