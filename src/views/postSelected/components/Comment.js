import React from 'react';
import { Col, Row } from 'react-bootstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Comment = (props) => {

    const { comment } = props;

    return (
        <Row
            className='mt-4'
            style={{ backgroundColor: 'white', borderRadius: '0.5rem' }}
        >
            <Col xs={12} className='mt-2'>
                <FontAwesomeIcon
                    style={{ fontSize: '1.8rem' }}
                    icon="user-circle"
                />
                <span
                    className='ml-2 align-top'
                    style={{ fontSize: '1.2rem' }}
                >
                    {comment.user}
                </span>
            </Col>
            <Col xs={11} className='ml-auto mr-auto mt-3'>
                <p className='text-justify'>
                    {comment.comment}
                </p>
            </Col>
        </Row>
    );
}

export default Comment;