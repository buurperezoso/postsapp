import React, { useCallback, useState } from 'react';
import { Row, Col, } from 'react-bootstrap';
import { TextField } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';

import { NewPostButton } from '../../../components/styles/GlobalComponentStyles';

import { devAPIurl, httpStatus } from '../../../Constants/Constants';
import { useDispatch } from 'react-redux';
import allActions from '../../../Redux/actions';

const AddComment = ({ postID }) => {

    const dispatch = useDispatch();

    // Form state
    const [comment, setComment] = useState('');

    // Http request state (controls button add disabled and label)
    const [httpReqState, setHttpReqState] = useState(httpStatus.NONE);

    //Form errors
    const [commentError, setCommentError] = useState(false);

    // Clears the form and states
    const resetForm = useCallback(() => {
        document.getElementById('commentForm').reset();
        setComment('');
        setCommentError(false);
        setHttpReqState(httpStatus.RESPONSE);
    }, []);

    // Http request handler
    const saveComment = useCallback((formData, postID) => {

        const url = `${devAPIurl}/post/${postID}/comment`;
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json;charset=UTF-8'
            },
            body: JSON.stringify(formData),
        };

        fetch(url, options)
            .then(response => {
                return response.json();
            }).then(
                (responseData) => {
                    dispatch(allActions.postActions.updatePost(responseData))
                    resetForm();
                }
            ).catch((error) => {
                console.error(error);
                setHttpReqState(httpStatus.RESPONSE);
            });
    }, [dispatch, resetForm]);

    // Form handler
    const handleSubmit = useCallback((event) => {
        event.preventDefault();

        let error = false;

        if (!comment) {
            setCommentError(true);
            error = true;
        } else {
            setCommentError(false);
        }

        if (!error) {
            const formData = {
                comment: comment,
                user: localStorage.getItem('user')
            }

            setHttpReqState(httpStatus.SENT);
            saveComment(formData, postID);
        }
    }, [comment, postID, saveComment]);

    return (
        <form autoComplete='off' onSubmit={handleSubmit} id='commentForm'>
            <Row className='mt-4'>
                <Col xs={12}>
                    <TextField
                        error={commentError}
                        fullWidth
                        label="Write a comment"
                        multiline
                        rows={4}
                        onChange={e => {
                            setComment(e.target.value);
                            if (e.target.value) {
                                setCommentError(false);
                            } else {
                                setCommentError(true);
                            }
                        }}
                        helperText={commentError ? 'This field is requiered.' : ''}
                    />
                </Col>
                <Col xs={12} className='mt-4'>
                    <NewPostButton
                        type='submit'
                        disabled={httpReqState === httpStatus.SENT}
                        style={{ width: '4rem' }}
                    >
                        {
                            httpReqState === httpStatus.SENT ? (
                                <CircularProgress
                                    size={'1.1rem'}
                                    style={{ color: 'white' }}
                                />
                            ) : 'Add'
                        }
                    </NewPostButton>
                </Col>
            </Row>
        </form>
    );
}

export default AddComment;