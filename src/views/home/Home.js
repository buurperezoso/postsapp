import React, { Fragment, useEffect, useState, useCallback } from 'react';
import { Col, Row, Button, Modal } from 'react-bootstrap';

import { categoryButtons } from '../../Constants/Constants';

import BlogPost from './components/BlogPost';
import AddButton from './components/AddButton';
import ModalNewPost from '../../components/ModalNewPost';
import ModalDeletePost from '../../components/ModalDeletePost';
import ModalEditPost from '../../components/ModalEditPost';

import {
    RecordNoneExisting,
    stylesImagesContainer,
    stylesButtonImage,
    stylesMenu
} from './styles/HomeStyles';

import styles from './styles/Home.module.css';

import loadingGradian from '../../components/styles/LoadingGradient.module.css';
import { useSelector } from 'react-redux';

const Home = (props) => {

    //Posts Array
    const propsPostList = useSelector(state => state.postListReducer.postList);

    // Buttons filter state
    const [activeButton, setActiveButton] = useState('ALL');

    // Post Array that will be shown (filter is applied in this variable)
    const [postList, setPostList] = useState([]);

    // Controls of NewModal, EditModal and DeleteModal
    const [showNewPost, setShowNewPost] = useState(false);
    const [showDeletePost, setShowDeletePost] = useState(false);
    const [showEditPost, setShowEditPost] = useState(false);
    const [manipulatedPostID, setManipulatedPostID] = useState('');
    const [editPostContent, setEditPostContent] = useState('');

    // Http request state
    const [httpReqState, setHttpReqState] = useState('SENT');

    useEffect(() => {

        document.title = 'Discovering the World';

        if (Array.isArray(propsPostList)) {
            setPostList(propsPostList);
            setActiveButton('ALL');
            setHttpReqState('RESPONSE');
        }

    }, [propsPostList]);

    // Filter array by the selected option
    const filterPostList = useCallback((category) => {

        let filteredObj;

        if (category === 'ALL') {
            filteredObj = propsPostList;
        } else {
            filteredObj = propsPostList.filter((post) => post.category === category);
        }

        setPostList(filteredObj);
        setActiveButton(category);

    }, [propsPostList]);

    // Modal New Post handlers
    const handleCloseNewPost = () => setShowNewPost(false);
    const handleShowNewPost = () => setShowNewPost(true);

    // Modal Delete Post handlers
    const handleCloseDeletePost = () => setShowDeletePost(false);
    const handleShowDeletePost = (id) => {
        setManipulatedPostID(id);
        setShowDeletePost(true);
    };

    // Modal Edit Post handlers
    const handleCloseEditPost = () => setShowEditPost(false);
    const handleShowEditPost = (post) => {
        setShowEditPost(true);
        setEditPostContent(post);
    };

    // Function control what it shows under de buttons menu
    const showContent = () => {

        let shownValue;

        if (httpReqState === 'SENT') {

            shownValue = (
                <Fragment>
                    <Col
                        md={6}
                        className={loadingGradian.gradientDiv}
                        style={{ ...stylesImagesContainer }}
                    ></Col>
                    <Col
                        md={6}
                        className={loadingGradian.gradientDiv}
                        style={{ ...stylesImagesContainer }}
                    ></Col>
                </Fragment>
            );

        } else if (postList.length > 0) {

            shownValue = postList.map(
                (post) => {
                    return (
                        <Col
                            md={6}
                            className={styles.ImageContainer}
                            style={{
                                backgroundImage: `url(${post.imgRoute}`,
                            }}
                            key={post.id}
                        >

                            <li
                                style={stylesButtonImage}
                                type='button'
                                onClick={() => props.history.push(`/post/${post.id}`)}
                            >
                                <BlogPost
                                    id={post.id}
                                    title={post.title}
                                    description={post.description}
                                    comments={post.comments.length}
                                    category={post.category}
                                    imgRoute={post.imgRoute}
                                    clickOnDelete={(id) => { handleShowDeletePost(id) }}
                                    clickOnEdit={(post) => { handleShowEditPost(post) }}
                                ></BlogPost>
                            </li>
                        </Col>
                    )
                }
            )

        } else {
            shownValue = (
                <div style={{ padding: '1rem', width: '100%' }}>
                    <RecordNoneExisting
                        style={{ fontSize: '1.5rem', color: '#9E9E9E' }}
                    >
                        No records found
                </RecordNoneExisting>
                </div>
            );
        }

        return shownValue;
    }

    return (
        <div className='pr-md-3 pl-md-3'>

            <Row>
                <Col style={{ textAlign: 'right' }}>
                    <AddButton clickShowModal={handleShowNewPost}></AddButton>
                </Col>
            </Row>

            <Row>
                <Col style={{
                    marginTop: '2rem',
                    paddingRight: 0,
                    paddingLeft: 0
                }}>
                    <Button
                        className={
                            activeButton === 'ALL' ? `active ${styles.leftButton}` : styles.leftButton
                        }
                        style={stylesMenu}
                        variant="light"
                        onClick={() => filterPostList('ALL')}
                    >
                        All
                    </Button>

                    {
                        categoryButtons.map((category, index) => {
                            switch (index) {
                                case (categoryButtons.length - 1):
                                    return (
                                        <Button
                                            className={
                                                activeButton === category.toUpperCase() ? `active ${styles.rightButton}` : styles.rightButton
                                            }
                                            style={stylesMenu}
                                            variant="light"
                                            key={index}
                                            onClick={() => filterPostList(category.toUpperCase())}
                                        >
                                            {category}
                                        </Button>
                                    );
                                default:
                                    return (
                                        <Button
                                            className={
                                                activeButton === category.toUpperCase() ? `active ${styles.centerButtons}` : styles.centerButtons
                                            }
                                            style={stylesMenu}
                                            variant="light"
                                            key={index}
                                            onClick={() => filterPostList(category.toUpperCase())}
                                        >
                                            {category}
                                        </Button>
                                    );
                            }

                        })

                    }

                </Col>
            </Row>

            <Row className='mt-4'>

                {
                    showContent()
                }

            </Row>

            {/* New Post Modal */}
            <Modal
                show={showNewPost}
                onHide={handleCloseNewPost}
                centered
                backdrop="static">
                <ModalNewPost
                    clickOnCancel={handleCloseNewPost}>
                </ModalNewPost>
            </Modal>

            {/* Delete Post Modal */}
            <Modal
                show={showDeletePost}
                onHide={handleCloseDeletePost}
                centered
                backdrop="static">
                <ModalDeletePost
                    clickOnCancel={handleCloseDeletePost}
                    postID={manipulatedPostID}>
                </ModalDeletePost>
            </Modal>

            {/* Edit Post Modal */}
            <Modal
                show={showEditPost}
                onHide={handleShowEditPost}
                centered
                backdrop="static">
                <ModalEditPost
                    clickOnCancel={handleCloseEditPost}
                    postObj={editPostContent}
                >
                </ModalEditPost>
            </Modal>

        </div>
    );

}

export default Home;