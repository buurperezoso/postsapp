import styled from 'styled-components';

export const AddButtonStyled = styled.button`
        display: inline-block;
        font-weight: 400;
        text-align: center;
        vertical-align: middle;
        user-select: none;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        border-radius: 50%;
        height: 3.5rem;
        width: 3.5rem;
        box-shadow: 0 0.4rem 0.3rem #9E9E9E;
        background-color: coral;
        color: white;
        transition: .4s;
        border: 0;
        outline: none!important;
        :hover {
            background-color: rgb(212, 109, 71);
            color: white;
            transform: scale(1.10);
        }
    `;

export const EditDeleteButton = styled.button`
        color: white;
        border-radius: 50%;
        :hover {
            color: white;
            background-color: #969696bf;
        }
    `;

export const RecordNoneExisting = styled.div`
        border: 1px solid #A0A0A0;
        box-shadow: 0 0.4rem 0.3rem #9E9E9E;
        border-radius: 0.5rem;
        height: 4rem;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-basis: 0;
        flex-grow: 1;
        max-width: 29rem;
        margin-left: auto;
        margin-right: auto;
    `;

export const stylesMenu = {
    borderColor: '#d3d9df',
    width: '6rem',
    height: '3rem',
}

export const stylesMenuCenter = {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
}

export const stylesImagesContainer = {
    backgroundColor: 'rgb(196 196 196 / 49%)',
    paddingRight: 0,
    paddingLeft: 0,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    cursor: 'pointer',
}

export const stylesButtonImage = {
    background: 'none',
    color: 'inherit',
    border: 'none',
    padding: 0,
    font: 'inherit',
    cursor: 'pointer',
    outline: 'inherit',
    width: '100%',
}