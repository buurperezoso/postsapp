import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { AddButtonStyled } from '../styles/HomeStyles';


const AddButton = (props) => {

    return (
        <AddButtonStyled onClick={props.clickShowModal}>
            <FontAwesomeIcon icon="pen" />
        </AddButtonStyled>
    );
}

export default AddButton;