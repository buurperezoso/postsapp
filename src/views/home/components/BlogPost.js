import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { EditDeleteButton } from '../styles/HomeStyles';

import styles from '../styles/Home.module.css';

const BlogPost = (props) => {

    const textStyle = {
        color: 'white',
        textAlign: 'left',
        width: '100%'
    }

    return (
        <div className={styles.blogPost}>
            <Row>

                <Col xs={12}>
                    <p className='text-break' style={{
                        fontSize: '3rem',
                        ...textStyle,
                    }}>
                        {props.title}
                    </p>
                </Col>

                <Col xs={12} className='mt-3'>
                    <p style={{
                        ...textStyle,
                        fontStyle: 'italic'
                    }}>
                        {props.comments} Comments <FontAwesomeIcon icon={['far', 'comments']} />
                    </p>
                </Col>

                <Col xs={12}>
                    <p style={{ ...textStyle }}>{props.description}</p>
                </Col>

                <Col xs={12} className='mt-3'>
                    <Row>
                        <Col xs={6}>
                            <p style={{ ...textStyle, letterSpacing: '4px' }}>{props.category}</p>
                        </Col>
                        <Col xs={6} className='d-flex justify-content-end'>

                            <EditDeleteButton
                                type='button'
                                className='btn'
                                onClick={(e) => {
                                    e.stopPropagation();
                                    props.clickOnEdit({
                                        id: props.id,
                                        title: props.title,
                                        description: props.description,
                                        category: props.category,
                                        imgRoute: props.imgRoute,
                                    })
                                }}
                            >
                                <FontAwesomeIcon icon='pen' />
                            </EditDeleteButton>

                            <EditDeleteButton
                                type='button'
                                className='btn'
                                onClick={(e) => {
                                    e.stopPropagation();
                                    props.clickOnDelete(props.id);
                                }}
                            >
                                <FontAwesomeIcon icon='trash' />
                            </EditDeleteButton>

                        </Col>
                    </Row>
                </Col>

            </Row>
        </div>
    );
}

export default BlogPost;