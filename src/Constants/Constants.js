export const categoryButtons = ['Travel', 'Lifestyle', 'Buisness', 'Food', 'Work'];

// export const devAPIurl = 'https://cors-anywhere.herokuapp.com/https://occce9gyk7.execute-api.us-west-1.amazonaws.com/dev';
export const devAPIurl = 'https://8bkvi37k6l.execute-api.us-west-1.amazonaws.com/dev';

export const urlRegex = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i;

export const reduxType = {
    SET: 'SET',
    ADD: 'ADD',
    UPDATE: 'UPDATE',
    DELETE: 'DELETE',
};

export const httpStatus = {
    NONE: 'NONE',
    SENT: 'SENT',
    RESPONSE: 'RESPONSE',
};