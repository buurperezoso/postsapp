import React, { useEffect } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Route, Switch } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import './App.css';

import Home from './views/home/Home';
import PostSelected from './views/postSelected/PostSelected';
import Login from './components/Login';

import { devAPIurl } from './Constants/Constants';
import allActions from './Redux/actions';

function App() {

  /* URL IMAGES FOR TEST */
  // 'https://images.unsplash.com/photo-1516918656725-e9b3ae9ee7a4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
  // 'https://eskipaper.com/images/high-resolution-photos-1.jpg'
  // 'https://images.unsplash.com/photo-1543373014-cfe4f4bc1cdf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
  // 'https://wallpaperaccess.com/full/405435.jpg'
  // 'https://i.redd.it/cjkz6lpzmprx.jpg'
  // 'https://images.firstpost.com/wp-content/uploads/2019/02/Moon.jpg'

  const dispatch = useDispatch();

  useEffect(() => {
    const url = `${devAPIurl}/posts`;
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
      },
    };

    fetch(url, options)
      .then(response => {
        return response.json();
      }).then(
        (responseData) => {
          dispatch(allActions.postActions.setPostList(responseData));
        }
      );
  }, [dispatch]);

  return (
    <div className="App">
      <Container fluid className='pb-md-3'>
        <Row>
          <Col className='loginContainer'>
            <Login />
          </Col>
        </Row>
        <Row>
          <Col style={{ marginTop: '2rem' }}>
            <span style={{ color: 'coral', fontSize: '2rem' }}>[</span>
            <span className='mx-2' style={{ color: 'coral' }}>Making your Life Easier</span>
            <span style={{ color: 'coral', fontSize: '2rem' }}>]</span>
          </Col>
        </Row>
        <Row>
          <Col>
            <span className='text-break' style={{ fontSize: '3rem', fontWeight: 'bold' }}>Discovering the World</span>
          </Col>
        </Row>

        <Switch>
          <Route
            path="/"
            exact
            component={Home}
          />
          <Route
            path="/post/:postID"
            component={PostSelected}
          />
        </Switch>

      </Container>

    </div>
  );
}

export default App;